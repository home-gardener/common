Backend
![https://gitlab.com/home-gardener/backend/pipelines](https://gitlab.com/home-gardener/backend/badges/master/pipeline.svg) ![https://gitlab.com/home-gardener/backend](https://gitlab.com/home-gardener/backend/badges/master/coverage.svg)

Frontend
![https://gitlab.com/home-gardener/frontend/pipelines](https://gitlab.com/home-gardener/frontend/badges/master/pipeline.svg) ![https://gitlab.com/home-gardener/frontend](https://gitlab.com/home-gardener/frontend/badges/master/coverage.svg)

A modern, React+GraphQL home gardening companion. I was not satisfied with any other existing solutions out there, so I brewed my own. Documentation is sparse, open an issue if you want to try to set this up for yourself and are having issues.

# Quickstart

Make a project folder, then clone the common, frontend, and backend repositories into it.

## Common

Common has the code that generates bindings from the backend for the frontend. Run `npm run generate-gql` in `common/` any time you change the graphql structure on the backend to keep it in sync.

## Backend

Primsa2-based. No documentation at this time.

## Frontend

React-based. No documentation at this time.
